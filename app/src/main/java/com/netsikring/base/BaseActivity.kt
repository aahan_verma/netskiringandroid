package com.netsikring.base

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewbinding.ViewBinding
import com.android.billingclient.api.*
import com.google.firebase.database.*
import com.netsikring.R
import com.netsikring.di.model.prefs.PrefUtils
import com.netsikring.model.GetTokenResponse
import com.netsikring.network.ApiClient
import com.netsikring.network.ApiConnector
import com.netsikring.network.ApiService
import com.netsikring.ui.netsikring.NetsikringFragment
import com.netsikring.utils.AppCommon
import com.netsikring.utils.NetsikringCommon
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import kotlin.collections.HashMap

abstract class BaseActivity<T : ViewBinding> : AppCompatActivity(),
    FragmentManager.OnBackStackChangedListener {

    var currentFragment: Fragment? = null

    @LayoutRes
    abstract fun getLayoutId(): Int

    private var mViewDataBinding: T? = null
    private var mPref: PrefUtils? = null
    open fun getViewDataBinding(): T {
        return mViewDataBinding!!
    }

    abstract fun getCurrentActivity(): Activity?

    fun setRunningFragment(frag0: Fragment?) {
        currentFragment = frag0
    }

    fun getNetsikringPrefs(): PrefUtils? {
        return mPref
    }

    var mHandler: Handler? = null

    private var service: ApiService? = null
    private val disposable = CompositeDisposable()

    /**
     * Subscription purchase
     **/
    var subscriptionBillingClient: BillingClient? = null
    var subscriptionBillingResult: BillingResult? = null
    var subSkuDetailsList: MutableList<SkuDetails> = ArrayList()

    private var instance: FirebaseDatabase? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mHandler = Handler()
        performDataBinding()
        if (subSkuDetailsList.size == 0) {
            setupSubscriptionClient()
        }
        if (instance == null) {
            instance = FirebaseDatabase.getInstance()
        }
        mPref = PrefUtils()
        service = ApiClient().getClient(this@BaseActivity).create(ApiService::class.java)

    }

    fun checkIfIdExist() {
        getFirebaseDBInstance().child(getEmail()).addListenerForSingleValueEvent(postListener)
    }

    fun getFirebaseDBInstance(): DatabaseReference {
        if (instance == null) {
            instance = FirebaseDatabase.getInstance()
        }
        val tableName = "${getString(R.string.app_name)}_Payment"
        return instance!!.getReference(tableName)
    }
    val postListener = object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
           val data = dataSnapshot.value
            Log.e("postListener","$data")
            getNetsikringPrefs()?.setTakenProgram(this@BaseActivity,data.toString())
        }

        override fun onCancelled(databaseError: DatabaseError) {

        }
    }
    fun getDisposable(): CompositeDisposable? {
        return disposable
    }

    fun getApiService(): ApiService? {
        return service
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }


    fun performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        try {
            val view = currentFocus
            if (view != null && (ev.action == MotionEvent.ACTION_UP
                        || ev.action == MotionEvent.ACTION_MOVE)
                && view is EditText && !view.javaClass.name.startsWith(
                    "android.webkit."
                )
            ) {
                val scrcoords = IntArray(2)
                view.getLocationOnScreen(scrcoords)
                val x = ev.rawX + view.getLeft() - scrcoords[0]
                val y = ev.rawY + view.getTop() - scrcoords[1]
                if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) (Objects.requireNonNull(
                    this.getSystemService(
                        INPUT_METHOD_SERVICE
                    )
                ) as InputMethodManager).hideSoftInputFromWindow(
                    this.window.decorView.applicationWindowToken, 0
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return super.dispatchTouchEvent(ev)
    }


    override fun onBackStackChanged() {
        val localFragmentManager = supportFragmentManager
        val i = localFragmentManager.backStackEntryCount
        if (i == 1 || i == 0) {
            finish()
        } else {
            mHandler!!.postDelayed({ localFragmentManager.popBackStack() }, 100)
        }
    }

    private var mDialogLoader: Dialog? = null
    fun showLoader() {
        try {
            hideLoader()
            mDialogLoader = NetsikringCommon.setLoadingDialog(this);
        }catch (e:Exception){
            e.printStackTrace()
        }

    }

    fun hideLoader() {
        if (mDialogLoader != null && mDialogLoader!!.isShowing) {
            mDialogLoader?.cancel()
        }
    }


    private fun setupSubscriptionClient() {
        try {
            subscriptionBillingClient = BillingClient.newBuilder(this)
                .enablePendingPurchases()
                .setListener(object : PurchasesUpdatedListener {
                    override fun onPurchasesUpdated(
                        billingResult: BillingResult,
                        purchases: MutableList<Purchase>?
                    ) {
                        if (billingResult?.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
                            for (purchase in purchases) {
                                acknowledgePurchase(purchase.purchaseToken)
                            }

                        } else if (billingResult?.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
                            showToast("You have cancelled this purchase please buy this product to move further")
                        }
                    }
                })
                .build()
            subscriptionBillingClient!!.startConnection(object : BillingClientStateListener {
                override fun onBillingSetupFinished(billingResult: BillingResult) {
                    if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                        subscriptionBillingResult = billingResult

                        loadBillingSku()
                        checkSubscriptionStatus()
                    }
                }

                override fun onBillingServiceDisconnected() {

                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showToast(s: String) {
        Toast.makeText(this@BaseActivity, s, Toast.LENGTH_LONG).show()
    }

    private fun acknowledgePurchase(purchaseToken: String) {
        runOnUiThread {
            val params = AcknowledgePurchaseParams.newBuilder()
                .setPurchaseToken(purchaseToken)
                .build()
            subscriptionBillingClient!!.acknowledgePurchase(params) { billingResult ->
                val responseCode = billingResult.responseCode
                val debugMessage = billingResult.debugMessage

                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    this@BaseActivity.runOnUiThread {
                        getNetsikringPrefs()?.setIsPaymentDone(
                            this@BaseActivity,
                            true
                        )!!

                        onPaymentCompleteInterface?.onComplete()

                      //  uploadPurchaseToken(selectedSKu)
                    }

                }

            }
        }
    }

    fun generateT(mCallback: NetsikringFragment.onTokenGenrateCallBack) {
        showLoader()
        val api = getApiService()!!.getToken()
        val connector = ApiConnector<GetTokenResponse>(
            this@BaseActivity,
            api,
            GetTokenResponse::class.java
        )
        connector.initApi(object : ApiConnector.onApiCallBack<GetTokenResponse> {
            override fun onSuccess(response: GetTokenResponse) {
                mCallback.getToken(response.token!!)
            }

            override fun onFailure(message: String) {
                hideLoader()
            }
        })
    }


    val subSkuList = listOf(
        "com.netsikring.permium", "com.netsikring.kombi_6",
        "com.netsikring.ultimate_20", "com.netsikring.kombi_3", "com.netsikring.kombi_1"
    )


    var onPaymentCompleteInterface: onPurchaseComplete? = null
    var selectedSKu = ""
    internal fun loadBillingSku() = if (subscriptionBillingClient!!.isReady) {
        val params = SkuDetailsParams
            .newBuilder()
            .setSkusList(subSkuList)
            .setType(BillingClient.SkuType.SUBS)
            .build()
        subscriptionBillingClient!!.querySkuDetailsAsync(params) { billingResult, skuDetailsList ->
            // Process the result.
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && skuDetailsList!!.isNotEmpty()) {
                for (skuDetails in skuDetailsList!!) {
                    this@BaseActivity.subSkuDetailsList = skuDetailsList
                }
            }
        }
    } else {
        println("Billing Client not ready")
    }

    interface onPurchaseComplete {
        fun onComplete()
    }

    public fun goForSubScriptingPurchase(SKU: String, callback: onPurchaseComplete) {
        runOnUiThread {
            this.onPaymentCompleteInterface = callback
        }
        if (subscriptionBillingResult!!.responseCode == BillingClient.BillingResponseCode.OK && subSkuDetailsList.isNotEmpty()) {
            for (skuDetails in subSkuDetailsList) {
                if (skuDetails.sku == SKU) {
                    selectedSKu = SKU
                    val billingFlowParams = BillingFlowParams
                        .newBuilder()
                        .setSkuDetails(skuDetails)
                        .build()
                    subscriptionBillingClient!!.launchBillingFlow(this, billingFlowParams)
                }
            }
        }

    }

    fun checkSubscriptionStatus() {
        try {
            val fastResetData = getNetsikringPrefs()?.getIsPaymentDone(this@BaseActivity)!!
            if (fastResetData) {
                if (subscriptionBillingClient!!.queryPurchases(BillingClient.SkuType.SUBS).purchasesList != null &&
                    subscriptionBillingClient!!.queryPurchases(BillingClient.SkuType.SUBS).purchasesList!!.size == 0
                ) {
                    val email = getEmail()
                    getFirebaseDBInstance().child(email).removeValue()
                    getNetsikringPrefs()?.setIsPaymentDone(
                        this@BaseActivity,
                        false
                    )!!
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getEmail(): String {
        val email = getNetsikringPrefs()?.getUserInformation(this@BaseActivity)?.id!!
        return email.toString()
    }
}