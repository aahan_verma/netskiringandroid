package com.netsikring.notification

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NotificationPayload {
    @SerializedName("body")
    @Expose
    var body: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("type")
    @Expose
    var type: String? = null

}