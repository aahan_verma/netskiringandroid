package com.netsikring.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.fragment.app.Fragment
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.netsikring.R
import com.netsikring.di.model.prefs.PrefUtils
import com.netsikring.dns.DNSPresenter
import com.netsikring.ui.home.HomeActivity
import org.json.JSONObject
import javax.inject.Inject

class MyFirebaseNotification : FirebaseMessagingService() {


    companion object {
        val TAG = MyFirebaseNotification::class.java.simpleName
        var notificationCount = 0
        var CurrentFrag: Fragment? = null
        const val NOTIFICATION_DATA = "x-notification-data"
        const val disabled = "disabled"
        var mCurrentActivity: AppCompatActivity? = null
        fun cancelNotification(ctx: Context) {
            val ns = NOTIFICATION_SERVICE
            val nMgr = ctx.getSystemService(ns) as NotificationManager
            nMgr.cancelAll()
        }
    }
    @Inject
    lateinit var presenter: DNSPresenter
    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        Log.e("onMessageReceived", p0.toString())
        if (p0.data.isNotEmpty()) {
            val params = p0.data
            val obj = JSONObject(params as Map<*, *>)
            if (obj.optString("type").equals(disabled, true)) {
                try {
                    val pref = PrefUtils()
                    pref?.setIsPaymentDone(this@MyFirebaseNotification, false)!!
                    pref?.isUserLoggedIn(this@MyFirebaseNotification, false)

                    if (mCurrentActivity is HomeActivity) {
                        (mCurrentActivity as HomeActivity).navigateToActivity()
                    }else{
                        pref?.setIsForceLogout(this@MyFirebaseNotification, true)
                    }
                    if (presenter.isWorking){
                        presenter.stopService()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            sendNotification(obj.optString("title") ?: "", obj.optString("body") ?: "")
        }

    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Log.e("onNewToken", p0)
        PrefUtils().setDeviceToken(this@MyFirebaseNotification, p0)
    }


    private fun sendNotification(title: String, content: String) {
        notificationCount++
        val intent = Intent(this, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.action = System.currentTimeMillis().toString()
        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_UPDATE_CURRENT
        )

        val channelId = getString(R.string.app_name)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(title)
            .setContentText(content)
            .setStyle(NotificationCompat.BigTextStyle().bigText(content))
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setContentIntent(pendingIntent)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                getString(R.string.app_name),
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(
            notificationCount/* ID of notification */,
            notificationBuilder.build()
        )
    }

}