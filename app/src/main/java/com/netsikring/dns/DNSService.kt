package com.netsikring.dns

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.VpnService
import android.os.Build
import android.os.ParcelFileDescriptor
import android.text.TextUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.preference.PreferenceManager
import com.android.billingclient.api.*
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.Gson
import com.netsikring.BuildConfig
import com.netsikring.R
import com.netsikring.application.MainApplication
import com.netsikring.di.model.prefs.PrefUtils
import com.netsikring.model.DNSModel
import com.netsikring.ui.home.HomeActivity
import com.netsikring.utils.RxBus
import com.netsikring.utils.event.GetServiceInfo
import com.netsikring.utils.event.ServiceInfo
import com.netsikring.utils.event.StartEvent
import com.netsikring.utils.event.StopEvent
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.io.IOException
import java.net.InetSocketAddress
import java.nio.channels.DatagramChannel
import java.util.ArrayList
import javax.inject.Inject

class DNSService : VpnService() {
    @Inject
    lateinit var rxBus: RxBus

    @Inject
    lateinit var context: Context

    @Inject
    lateinit var gson: Gson
    private val builder: Builder = Builder()
    private val CHANNEL_ID = "NETSIKRING_CHANNEL"
    private var fileDescriptor: ParcelFileDescriptor? = null
    private var mThread: Thread? = null
    private var shouldRun = true
    private var tunnel: DatagramChannel? = null
    private var dnsModel: DNSModel? = null
    private var preferences: SharedPreferences? = null
    private var subscriber: Disposable? = null
    private fun stopThisService() {
        shouldRun = false
        stopSelf()
    }

    var subscriptionBillingClient: BillingClient? = null
    var subscriptionBillingResult: BillingResult? = null
    val subSkuList = listOf(
        "com.netsikring.permium", "com.netsikring.kombi_6",
        "com.netsikring.ultimate_20", "com.netsikring.kombi_3", "com.netsikring.kombi_1"
    )
    var subSkuDetailsList: MutableList<SkuDetails> = ArrayList()
    override fun onDestroy() {
        stopForeground(true)
        shouldRun = false;
        mThread = null;
        subscriber!!.dispose()
        super.onDestroy()
        /* preferences!!.edit().putBoolean("isStarted", false).apply()
         preferences!!.edit().remove("dnsModel").apply()

         subscriber!!.dispose()*/

    }

    override fun onCreate() {
        super.onCreate()
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        MainApplication.getApplicationComponent()?.inject(this)
        createSubClient()
        mPref = PrefUtils()
        subscribe()
    }

    private fun createSubClient() {
        try {
            subscriptionBillingClient = BillingClient.newBuilder(this)
                .enablePendingPurchases()
                .setListener { billingResult, purchases ->
                    if (billingResult?.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {

                    } else if (billingResult?.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
                        // showToast("You have cancelled this purchase please buy this product to move further")
                    }
                }
                .build()
            subscriptionBillingClient?.startConnection(object : BillingClientStateListener {
                override fun onBillingSetupFinished(billingResult: BillingResult) {
                    if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                        subscriptionBillingResult = billingResult

                        loadBillingSku()
                        checkSubscriptionStatus()
                    }
                }

                override fun onBillingServiceDisconnected() {

                }
            })
        }catch (e:Exception){
            e.printStackTrace()
        }

    }

    private var mPref: PrefUtils? = null
    private fun checkSubscriptionStatus() {
        try {
            val fastResetData = mPref?.getIsPaymentDone(this@DNSService)!!
            if (fastResetData) {
                if (subscriptionBillingClient!!.queryPurchases(BillingClient.SkuType.SUBS).purchasesList != null &&
                    subscriptionBillingClient!!.queryPurchases(BillingClient.SkuType.SUBS).purchasesList!!.size == 0
                ) {
                    val email = getEmail()
                    val instance = FirebaseDatabase.getInstance()
                    val tableName = "${getString(R.string.app_name)}_Payment"
                    instance!!.getReference(tableName).child("$email").removeValue()
                    mPref?.setIsPaymentDone(
                        this@DNSService,
                        false
                    )!!
                    mPref?.isUserLoggedIn(this@DNSService,false)
                    stopThisService()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getEmail(): String {
        /*  val email = getNetsikringPrefs()?.getEmail(this@BaseActivity)!!.replace("@", "")
              .replace(".", "").replace("{", "").replace("]", "")
              .replace("-", "").replace("/", "").replace("*", "")
              .replace("/", "").replace("<", "").replace(">", "")
              .replace("{", "").replace("}", "")*/
        val email = mPref?.getUserInformation(this@DNSService)?.id!!
        return email.toString()
    }

    internal fun loadBillingSku() = if (subscriptionBillingClient!!.isReady) {
        val params = SkuDetailsParams
            .newBuilder()
            .setSkusList(subSkuList)
            .setType(BillingClient.SkuType.SUBS)
            .build()
        subscriptionBillingClient!!.querySkuDetailsAsync(params) { billingResult, skuDetailsList ->
            // Process the result.
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && skuDetailsList!!.isNotEmpty()) {
                for (skuDetails in skuDetailsList!!) {
                    this@DNSService.subSkuDetailsList = skuDetailsList
                }
            }
        }
    } else {
        println("Billing Client not ready")
    }

    private fun subscribe() {
        subscriber = rxBus?.events.subscribe(Consumer<Any?> { o ->
            if (o is StopEvent) {
                stopThisService()
            } else if (o is GetServiceInfo) {
                rxBus?.sendEvent(ServiceInfo(dnsModel!!))
            }
        })
    }

    private fun setTunnel(tunnel: DatagramChannel) {
        this.tunnel = tunnel
    }

    private fun setFileDescriptor(fileDescriptor: ParcelFileDescriptor?) {
        this.fileDescriptor = fileDescriptor
        if (this.fileDescriptor != null) {
            Log.e("SHAGUN", "NNN " + this.fileDescriptor!!.fileDescriptor.toString());
        } else {
            Log.e("SHAGUN", "NULLL");
        }
    }

    private fun setNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name = getString(R.string.app_name)
            val descriptionText = getString(R.string.app_name)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
            mChannel.description = descriptionText
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

            if (notificationManager.getNotificationChannel(CHANNEL_ID) == null) {
                notificationManager.createNotificationChannel(mChannel)
            }

            val notificationIntent = Intent(this, HomeActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)
            val foregroundServiceNotificationTitle = context.getString(R.string.app_name)

            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(foregroundServiceNotificationTitle)
                .setContentText("")
                .setSmallIcon(R.drawable.ic_baseline_apps_24)
                .setContentIntent(pendingIntent)
                .build()

            startForeground(999, notification)
        }
    }

    fun fetchDnsModel() {
        val dnsModelJSON: String = preferences?.getString("dnsModel", "")!!
        if (!TextUtils.isEmpty(dnsModelJSON)) {
            dnsModel = Gson().fromJson(dnsModelJSON, DNSModel::class.java)

        }
    }

    fun setupConnection() {
        val intent = prepare(this)
        if (intent == null) {
            setFileDescriptor(
                builder.setSession(
                    this@DNSService.getText(R.string.app_name).toString()
                ).addAddress("192.168.0.1", 24).addDnsServer(dnsModel?.firstDns!!)
                    .addDnsServer(dnsModel!!.secondDns!!).establish()
            )
            if (BuildConfig.DEBUG) {
                Log.e("SSS", "Starting");
            }
            setTunnel(DatagramChannel.open())
            tunnel!!.connect(InetSocketAddress("127.0.0.1", 8087))
            protect(tunnel!!.socket())
            if (BuildConfig.DEBUG) {
                Log.e("SSS", "Set");
            }
        } else {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    override fun onStartCommand(paramIntent: Intent, p1: Int, p2: Int): Int {
        if (mThread == null) {
            setNotification()
            dnsModel = paramIntent.getParcelableExtra(DNS_MODEL)
            if (dnsModel != null) {
                Log.e("SSS", dnsModel.toString())
            } else {
                fetchDnsModel()
            }
            mThread = Thread({
                try {
                    val modelJSON: String = gson!!.toJson(dnsModel)
                    preferences!!.edit().putString("dnsModel", modelJSON).apply()
                    Log.e("SHAGUN", "CREATING");
                    setupConnection()
                    while (shouldRun) {
                        Thread.sleep(100L)
                        //Log.e("SHAGUN", "FILE $fileDescriptor")
                        if (this@DNSService.fileDescriptor == null) {
                            Thread.sleep(2000L)
                            this@DNSService.setupConnection()
                        }
                        checkSubscriptionStatus()
                    }
                } catch (exception: Exception) {
                    exception.printStackTrace()
                    if (BuildConfig.DEBUG) {
                        Log.e("SSS", "Starting error", exception);
                    }
                    stopThisService()
                } finally {
                    if (fileDescriptor != null) {
                        try {
                            fileDescriptor?.close()
                            setFileDescriptor(null)
                        } catch (e: IOException) {
                            if (BuildConfig.DEBUG) {
                                Log.e("SSS", "Starting error", e);
                            }
                        }
                    }
                }
            }, "DNS Changer")
            mThread!!.start()
            rxBus?.sendEvent(StartEvent())
            preferences!!.edit().putBoolean("isStarted", true).apply()
        }
        return START_REDELIVER_INTENT
    }

    companion object {
        const val DNS_MODEL = "DNSModelIntent"
    }
}