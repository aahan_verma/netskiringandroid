package com.netsikring.dns

import android.app.NotificationManager
import android.content.*
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.Build
import android.text.TextUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.netsikring.model.DNSModel
import com.netsikring.network.ApiConnector
import com.netsikring.ui.home.HomeActivity


class BootReceiver : BroadcastReceiver() {

    companion object {
        var receiver: BootReceiver = BootReceiver()
    }
    private var notificationManager: NotificationManager? = null
    private var notificationBuilder: NotificationCompat.Builder? = null

    override fun onReceive(context: Context, intent: Intent) {
        Log.e("SHAGUN", "RECV " + intent.getAction());
        if (ApiConnector.isOnline(context)) {
            Log.e("SHAGUN", "ONLINE");
            val preferences: SharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context)
            val autoStart: Boolean = preferences.getBoolean("startBoot", true)
            val isStarted: Boolean = preferences.getBoolean("isStarted", false)
            val dnsModelJSON: String = preferences.getString("dnsModel", "")!!
            if (!TextUtils.isEmpty(dnsModelJSON) && Intent.ACTION_BOOT_COMPLETED == intent.getAction()) {
                val intent = Intent(context, DNSService::class.java)
                val dnsModel = Gson().fromJson(dnsModelJSON, DNSModel::class.java)
                if (dnsModel != null) {
                    Log.e("SHAGUN", "STARTIING");
                    intent.putExtra(DNSService.DNS_MODEL, dnsModel)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        context.startForegroundService(intent)
                    } else {
                        context.startService(intent)
                    }
                }
            }
        } else {
            Log.e("SHAGUN", "ADDING");
            context.applicationContext.unregisterReceiver(receiver)
            val filter = IntentFilter()
            filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION)
            filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION)
            filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
            context.applicationContext.registerReceiver(receiver, filter)
        }

    }


    private fun sendNotification(context: Context, dnsModel: String, intent: Intent) {
        /*  val intentAction = Intent(context, HomeActivity::class.java)
          intentAction.putExtra("dnsModel", dnsModel)
          val pendingIntent: PendingIntent =
              PendingIntent.getActivity(context, 1, intentAction, PendingIntent.FLAG_ONE_SHOT)
          notificationBuilder = NotificationCompat.Builder(context)
              .setSmallIcon(R.drawable.ic_launcher_background)
              .setContentTitle(context.getString(R.string.service_ready))
              .setContentIntent(pendingIntent)
              .addAction(
                  R.mipmap.ic_launcher,
                  context.getString(R.string.turn_on),
                  pendingIntent
              )
              .setAutoCancel(true)
          val notification: Notification = notificationBuilder!!.build()
          notificationManager!!.notify(1903, notification)*/
        if (Intent.ACTION_BOOT_COMPLETED == intent.getAction()) {
            val activityIntent = Intent(context, HomeActivity::class.java)
            activityIntent.putExtra("dnsModel", dnsModel)
            activityIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(activityIntent)
        }
    }
}