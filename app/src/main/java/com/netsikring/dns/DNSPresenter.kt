package com.netsikring.dns

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Build
import com.netsikring.dns.DNSService.Companion.DNS_MODEL
import com.netsikring.model.DNSModel
import com.netsikring.utils.RxBus
import com.netsikring.utils.event.GetServiceInfo
import com.netsikring.utils.event.ServiceInfo
import com.netsikring.utils.event.StartEvent
import com.netsikring.utils.event.StopEvent
import io.reactivex.functions.Consumer
import javax.inject.Inject

class DNSPresenter @Inject constructor(
    private val view: IDNSView,
    rxBus: RxBus,
    context: Context
) {
    private val rxBus: RxBus
    private val context: Context
    private fun subscribe() {
        rxBus.events.subscribe(Consumer<Any> { o ->
            if (o is StartEvent) {
                view.changeStatus(SERVICE_OPEN)
            } else if (o is StopEvent) {
                view.changeStatus(SERVICE_CLOSE)
            } else if (o is ServiceInfo) {
                view.setServiceInfo((o as ServiceInfo).getModel())
            }
        })
    }

    fun stopService() {
        rxBus.sendEvent(StopEvent())
    }

    fun startService(dnsModel: DNSModel?) {
        val intent = Intent(context, DNSService::class.java)
        intent.putExtra(DNS_MODEL, dnsModel)
        view.setServiceInfo(dnsModel)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent)
        } else {
            context.startService(intent)
        }
    }

    val isWorking: Boolean
        get() {
            val manager: ActivityManager =
                context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val serviceName = DNSService::class.java.name
            for (service in manager.getRunningServices(Int.MAX_VALUE)) {
                if (serviceName == service.service.className) {
                    return true
                }
            }
            return false
        }
    val serviceStatus: Unit
        get() {
            if (isWorking) {
                serviceInfo
                view.changeStatus(SERVICE_OPEN)
            } else {
                view.changeStatus(SERVICE_CLOSE)
            }
        }
    val serviceInfo: Unit
        get() {
            rxBus.sendEvent(GetServiceInfo())
        }

    companion object {
        const val SERVICE_OPEN = 1
        const val SERVICE_CLOSE = 0
    }

    init {
        this.rxBus = rxBus
        this.context = context
        subscribe()
    }
}