package com.netsikring.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class DNSModel : Parcelable {
    @SerializedName("name")
    var name: String? = null

    @SerializedName("firstDNS")
    var firstDns: String? = null

    @SerializedName("secondDNS")
    var secondDns: String? = null
    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(name)
        dest.writeString(firstDns)
        dest.writeString(secondDns)
    }

    override fun toString(): String {
        return "DNSModel(name=$name, firstDns=$firstDns, secondDns=$secondDns)"
    }

    constructor() {}
    protected constructor(`in`: Parcel) {
        name = `in`.readString()
        firstDns = `in`.readString()
        secondDns = `in`.readString()
    }

    companion object CREATOR : Parcelable.Creator<DNSModel> {
        override fun createFromParcel(parcel: Parcel): DNSModel {
            return DNSModel(parcel)
        }

        override fun newArray(size: Int): Array<DNSModel?> {
            return arrayOfNulls(size)
        }
    }


}