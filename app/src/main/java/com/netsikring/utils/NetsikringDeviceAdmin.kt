package com.netsikring.utils

import android.app.admin.DeviceAdminReceiver
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.UserManager
import android.util.Log
import android.widget.Toast
import com.netsikring.R
import com.netsikring.ui.home.HomeActivity
import com.netsikring.utils.DeviceOwnerReceiver.Companion.getComponentName
import com.netsikring.utils.devicelock.AdminReceiver


/**
 * This is the component that is responsible for actual device administration.
 * It becomes the receiver when a policy is applied. It is important that we
 * subclass DeviceAdminReceiver class here and to implement its only required
 * method onEnabled().
 */
class NetsikringDeviceAdmin : DeviceAdminReceiver() {
    override fun onProfileProvisioningComplete(context: Context, intent: Intent) {
        // Enable the profile
        val manager = context.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        val componentName = getComponentName(context)
        manager.setProfileName(componentName, context.getString(R.string.app_name))

        val cn = ComponentName(context, AdminReceiver::class.java)
        manager!!.addUserRestriction(cn, UserManager.DISALLOW_UNINSTALL_APPS);
        // Open the main screen
        val launch = Intent(context, HomeActivity::class.java)
        launch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(launch)
    }
    /*  @Override
    public void onProfileProvisioningComplete(@NonNull Context context, @NonNull Intent intent) {
        super.onProfileProvisioningComplete(context, intent);
        Intent launch = Intent(context, HomeActivity);
        launch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(launch)
    }*/
    /** Called when this application is approved to be a device administrator.  */
    override fun onEnabled(context: Context, intent: Intent) {
        super.onEnabled(context, intent)
        Toast.makeText(
            context, "device admin enabled",
            Toast.LENGTH_LONG
        ).show()
        Log.d(TAG, "onEnabled")
    }

    /** Called when this application is no longer the device administrator.  */
    override fun onDisabled(context: Context, intent: Intent) {
        super.onDisabled(context, intent)
        Toast.makeText(
            context, "device admin disabled",
            Toast.LENGTH_LONG
        ).show()
        Log.d(TAG, "onDisabled")
    }

    override fun onPasswordChanged(context: Context, intent: Intent) {
        super.onPasswordChanged(context, intent)
        Log.d(TAG, "onPasswordChanged")
    }

    override fun onPasswordFailed(context: Context, intent: Intent) {
        super.onPasswordFailed(context, intent)
        Log.d(TAG, "onPasswordFailed")
    }

    override fun onPasswordSucceeded(context: Context, intent: Intent) {
        super.onPasswordSucceeded(context, intent)
        Log.d(TAG, "onPasswordSucceeded")
    }

    companion object {
        const val TAG = "DemoDeviceAdmin"
    }
}