package com.netsikring.utils

import android.app.admin.DeviceAdminReceiver
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.UserHandle
import android.widget.Toast
import com.netsikring.R
import com.netsikring.ui.home.HomeActivity

/**
 * Handles events related to device owner.
 */
class DeviceOwnerReceiver : DeviceAdminReceiver() {
    /**
     * Called on the new profile when device owner provisioning has completed. Device owner
     * provisioning is the process of setting up the device so that its main profile is managed by
     * the mobile device management (MDM) application set up as the device owner.
     */
   /* override fun onProfileProvisioningComplete(context: Context, intent: Intent) {
        // Enable the profile
        val manager = context.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        val componentName = getComponentName(context)
        manager.setProfileName(componentName, context.getString(R.string.app_name))
        // Open the main screen
        val launch = Intent(context, HomeActivity::class.java)
        launch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(launch)
    }*/
    override fun onEnabled(context: Context, intent: Intent) =
        showToast(context, "Enabled")



    override fun onDisabled(context: Context, intent: Intent) =
        showToast(context, "Disabled")

    override fun onPasswordChanged(context: Context, intent: Intent, userHandle: UserHandle) =
        showToast(context,"Password Changes")
    companion object {
        /**
         * @return A newly instantiated [android.content.ComponentName] for this
         * DeviceAdminReceiver.
         */
        fun getComponentName(context: Context): ComponentName {
            return ComponentName(context.applicationContext, DeviceOwnerReceiver::class.java)
        }
    }

    private fun showToast(context: Context, msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }
}