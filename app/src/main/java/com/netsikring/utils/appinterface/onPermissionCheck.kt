package com.netsikring.utils.appinterface

interface onPermissionCheck {
    fun permissionGranted()
}