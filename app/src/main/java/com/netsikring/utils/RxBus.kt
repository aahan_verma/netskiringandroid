package com.netsikring.utils

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBus {
    private val subject = PublishSubject.create<Any>()

    /**
     * Pass any event down to event listeners.
     */
    fun sendEvent(obj: Any) {
        subject.onNext(obj)
    }

    /**
     * Subscribe to this Observable. On event, do something
     * e.g. replace a fragment
     */
    val events: Observable<Any>
        get() = subject

    companion object {
        lateinit var instance: RxBus
        fun instanceOf(): RxBus? {
            if (instance == null) {
                instance = RxBus()
            }
            return instance
        }
    }
}