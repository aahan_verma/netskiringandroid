package com.netsikring.utils

import android.content.Context
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.netsikring.R
import android.content.Context.TELEPHONY_SERVICE
import android.os.Build
import android.provider.Settings

import androidx.core.content.ContextCompat.getSystemService

import android.telephony.TelephonyManager


object AppCommon {
    fun showExitWarning(layout: ViewGroup, activity: AppCompatActivity) {
        val snack = Snackbar.make(
            layout,
            "To exit, Tap back button again.",
            Snackbar.LENGTH_SHORT
        ).setBackgroundTint(ContextCompat.getColor(activity, R.color.purple_700))
        snack.setActionTextColor(ContextCompat.getColor(activity, R.color.white))
        snack.setAction("EXIT NOW") {
            activity.finish()
        }
        val snackBarView = snack.view
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity, R.color.purple_700))
        snack.show()
    }

    fun getDeviceID(mCtx: Context): String {
        return Settings.Secure.getString(mCtx.contentResolver,
            Settings.Secure.ANDROID_ID)
    }

}