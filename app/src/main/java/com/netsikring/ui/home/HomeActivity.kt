package com.netsikring.ui.home

import android.app.Activity
import android.app.Dialog
import android.app.NotificationManager
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.net.VpnService
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Html
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.preference.PreferenceManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import com.netsikring.R
import com.netsikring.application.MainApplication
import com.netsikring.base.BaseActivity
import com.netsikring.databinding.*
import com.netsikring.dns.DNSModule
import com.netsikring.dns.DNSPresenter
import com.netsikring.dns.DaggerDNSComponent
import com.netsikring.dns.IDNSView
import com.netsikring.model.DNSModel
import com.netsikring.model.GetConfigurationResponse
import com.netsikring.model.GetTokenResponse
import com.netsikring.model.LoginResponse
import com.netsikring.network.ApiConnector
import com.netsikring.notification.MyFirebaseNotification
import com.netsikring.ui.SubscriptionActivity
import com.netsikring.ui.changepassword.ActivityChangePassword
import com.netsikring.ui.netsikring.NetsikringActivity
import com.netsikring.ui.netsikring.NetsikringDialog
import com.netsikring.ui.netsikring.NetsikringFragment
import com.netsikring.utils.*
import com.ntpc.app.base.DialogBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap


class HomeActivity : BaseActivity<FragmentHomeBinding>(), IDNSView {
    override fun getLayoutId(): Int {
        return R.layout.fragment_home
    }

    private lateinit var dpm: DevicePolicyManager
    private lateinit var deviceAdminSample: ComponentName
    override fun getCurrentActivity(): Activity? {
        return this@HomeActivity
    }

    @Inject
    lateinit var presenter: DNSPresenter


    @Inject
    lateinit var gson: Gson
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val isForceLogout = getNetsikringPrefs()?.getIsForceLogout(this@HomeActivity)!!
        if (!isForceLogout) {
            dpm = getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
            deviceAdminSample = ComponentName(this@HomeActivity, NetsikringDeviceAdmin::class.java)
            DaggerDNSComponent.builder()
                .applicationComponent(MainApplication.getApplicationComponent())
                .dNSModule(DNSModule(this)).build().inject(this)
            getServiceStatus()
            parseIntent()
            checkIfIdExist()
            val paymentStatus = getNetsikringPrefs()?.getIsPaymentDone(this@HomeActivity)!!
            if (!paymentStatus) {
                checkOnFirebase()
            }

            getViewDataBinding().cbConnection.setOnClickListener {
                if (!dpm.isAdminActive(deviceAdminSample)) {
                    showAdminPermissionNeeded()
                } else {
                    startConnection()
                }
                //startActivity(Intent(this@HomeActivity,SubscriptionActivity::class.java))
            }
            getViewDataBinding().ivNav.setOnClickListener {
                optionsDialog()
            }
        } else {
            if (presenter.isWorking) {
                presenter.stopService()
            }
            getNetsikringPrefs()?.setIsForceLogout(this@HomeActivity, false)!!
            navigateToActivity()
        }
        /*if (preferences!!.getBoolean("isStarted", false)) {
            getViewDataBinding().cbConnection.performClick()
        }*/
    }

    fun startConnection() {
        if (differenceBetweenDate() >= 7) {
            if (getNetsikringPrefs()?.getIsPaymentDone(this@HomeActivity)!!) {
                startDNS()
            } else {
                startActivity(Intent(this@HomeActivity, SubscriptionActivity::class.java))
            }

        } else {
            startDNS()
        }
    }

    private fun checkifConected() {
        val preferences: SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(this@HomeActivity)
        val autoStart: Boolean = preferences.getBoolean("startBoot", true)
        val isStarted: Boolean = preferences.getBoolean("isStarted", false)
        if (isStarted) {
            if (!presenter.isWorking) {
                presenter.startService(getDnsModel())
            }
        }

    }

    companion object {
        var isPaymentDone = false
    }

    private fun checkOnFirebase() {
        val email = getEmail()
        getFirebaseDBInstance().child(email).addListenerForSingleValueEvent(object :
            ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                isPaymentDone = snapshot.value != null
                getNetsikringPrefs()?.setIsPaymentDone(this@HomeActivity, isPaymentDone)!!
            }

            override fun onCancelled(error: DatabaseError) {
                isPaymentDone = false
            }
        })
    }


    private fun getServiceStatus() {
        if (presenter.isWorking) {
            serviceStarted()
            presenter.serviceInfo
            Log.e("getServiceStatus", "Service Working")
        } else {
            serviceStopped()
            Log.e("getServiceStatus", "Service Stopped")
        }
    }

    private fun parseIntent() {
        if (intent != null && intent.extras != null) {
            try {
                val notificationManager =
                    getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.cancel(1903)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            runOnUiThread {
                try {
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                startDNS()
            }

        } else {
            checkifConected()
        }
    }

    private fun serviceStopped() {
        getViewDataBinding().tvConnectionStatus.text = "Tilslut"
        getViewDataBinding().internetStatus.hide()
    }

    private fun serviceStarted() {
        getViewDataBinding().tvConnectionStatus.text = "Tilsluttet."
        getViewDataBinding().internetStatus.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == REQUEST_CODE_ENABLE_ADMIN) {
            startConnection()
        } else {
            if (resultCode == -1) {
                startConnection()
            }
        }
    }

    private fun getDnsModel(): DNSModel {
        val dnsModel = DNSModel()
        var first = ""
        var second = ""
        getNetsikringPrefs()!!.getConfigurations(this@HomeActivity)!!.list!!.withIndex().map {
            if (it.value.keyName!!.equals("dns_primary", true)) {
                first = it.value.value!!
            }
            if (it.value.keyName!!.equals("dns_secondary", true)) {
                second = it.value.value!!
            }
        }

        if (first == "" || second == "") {
            first = "212.237.101.210"
            second = "212.237.101.210"
        }
        dnsModel.name = (getString(R.string.custom_dns))


        dnsModel.firstDns = first
        dnsModel.secondDns = second

        return dnsModel

    }

    internal fun startDNS() {
        if (presenter.isWorking) {
            try {
                checkIfPinIsChanged()

            } catch (e: Exception) {
                e.printStackTrace()
                // (currentFragment as HomeFragment).serviceStopped()
            }

        } else {
            val intent = VpnService.prepare(this)
            if (intent != null) {
                startActivityForResult(intent, REQUEST_CONNECT)
            } else {
                onActivityResult(REQUEST_CONNECT, RESULT_OK, null)
            }
        }
    }

    private fun updateServiceStatus(vpnStaus: String) {
        generateToken(object : NetsikringFragment.onTokenGenrateCallBack {
            override fun getToken(token: String) {
                val map = HashMap<String, Any>()
                map["_token"] = token
                map["user_id"] =
                    getNetsikringPrefs()!!.getUserInformation(this@HomeActivity)!!.id.toString()
                map["vpn_status"] = vpnStaus
                map["device_id"] = AppCommon.getDeviceID(this@HomeActivity)
                val api = getApiService()!!.updateVpnStatus(map)
                val connector = ApiConnector<GetTokenResponse>(
                    this@HomeActivity,
                    api,
                    GetTokenResponse::class.java
                )
                connector.initApi(object : ApiConnector.onApiCallBack<GetTokenResponse> {
                    override fun onSuccess(response: GetTokenResponse) {
                        if (response.success!!) {
                            getConfigurationApi(vpnStaus)
                        } else {
                            hideLoader()
                            showToast(response.message ?: response.error ?: "")
                        }
                    }

                    override fun onFailure(message: String) {
                        hideLoader()
                    }
                })
            }
        })
    }

    private fun getConfigurationApi(vpnStaus: String) {
        if (vpnStaus.equals("1")) {
            val api = getApiService()!!.getConfiguration()
            val connector = ApiConnector<GetConfigurationResponse>(
                this@HomeActivity,
                api,
                GetConfigurationResponse::class.java
            )
            connector.initApi(object : ApiConnector.onApiCallBack<GetConfigurationResponse> {
                override fun onSuccess(response: GetConfigurationResponse) {
                    hideLoader()
                    getNetsikringPrefs()!!.setConfigurations(
                        this@HomeActivity,
                        response.configurations!!
                    )
                    showStatusDialog(vpnStaus)
                }

                override fun onFailure(message: String) {
                    hideLoader()

                }
            })
        } else {
            hideLoader()
            showStatusDialog(vpnStaus)
        }
    }

    override fun onResume() {
        super.onResume()
        MyFirebaseNotification.mCurrentActivity = this@HomeActivity
    }

    override fun onStop() {
        super.onStop()
        MyFirebaseNotification.mCurrentActivity = null
    }

    private fun showStatusDialog(vpnStaus: String) {
        if (vpnStaus.equals("1")) {
            presenter.startService(getDnsModel())
            showToast("Tilsluttet Du kan nu lukke appen Appen beskytter i baggrunden")
            getNetsikringPrefs()!!.getConfigurations(this@HomeActivity)!!.list!!.withIndex().map {
                if (it.value.keyName!!.equals("company_info", true)) {
                    showInformationDialog(it.value.value!!)

                }
            }
        } else {
            if (dpm.isAdminActive(deviceAdminSample)) {
                dpm.removeActiveAdmin(deviceAdminSample)
            }
            val preferences: SharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(this@HomeActivity)
            preferences!!.edit().putBoolean("isStarted", false).apply()
            presenter.stopService()
            showToast("Beskyttelsesfilter opdateret til seneste version")
            getNetsikringPrefs()!!.getConfigurations(this@HomeActivity)!!.list!!.withIndex().map {
                if (it.value.keyName!!.equals("info_page_secondary", true)) {
                    showInformationDialog(it.value.value!!)

                }
            }
        }
    }

    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if (currentFragment is HomeFragment) {
            if (doubleBackToExitPressedOnce) {
                finish()
            }
            this.doubleBackToExitPressedOnce = true
            AppCommon.showExitWarning(getViewDataBinding().rlToolbar, this)

            Handler(Looper.getMainLooper()).postDelayed({
                doubleBackToExitPressedOnce = false
            }, 2000)
        } else {
            onBackStackChanged()
        }
    }


    internal fun showLogoutDialog() {
        NetsikringDialog<DialogLogoutBinding>(
            this@HomeActivity,
            R.layout.dialog_logout,
            object : DialogBinding<DialogLogoutBinding> {
                override fun onBind(binder: DialogLogoutBinding, dialog: Dialog) {
                    binder.btnYes.setOnClickListener {
                        dialog.dismiss()
                        getNetsikringPrefs()?.setIsPaymentDone(this@HomeActivity, false)!!
                        getNetsikringPrefs()!!.isUserLoggedIn(this@HomeActivity, false)
                        navigateToActivity()
                    }
                    binder.btnNotNow.setOnClickListener {
                        dialog.dismiss()
                    }
                }
            }).show(
            supportFragmentManager,
            NetsikringDialog::class.java.simpleName
        )

    }

    fun showAdminPermissionNeeded() {
        NetsikringDialog<DialogSubscriptionBoughtBinding>(
            this@HomeActivity,
            R.layout.dialog_subscription_bought,
            object : DialogBinding<DialogSubscriptionBoughtBinding> {
                override fun onBind(binder: DialogSubscriptionBoughtBinding, dialog: Dialog) {
                    binder.tvMessage.text =
                        "We need admin permission for using VPN functionality. Please click on \'Allow\' to proceed."
                    binder.btnClick.text= "Allow"
                    binder.btnClick.setOnClickListener {
                        dialog.dismiss()
                        startAdminRequest()
                    }
                    binder.tvNowNow.setOnClickListener {
                        dialog.dismiss()
                    }
                }
            }).show(supportFragmentManager, NetsikringDialog::class.java.simpleName)

    }

    fun navigateToActivity() {
        val intent = Intent(this@HomeActivity, NetsikringActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finishAffinity()
    }

    private val REQUEST_CONNECT = 21
    private val IP_PATTERN = Patterns.IP_ADDRESS
    override fun changeStatus(serviceStatus: Int) {
        if (serviceStatus == DNSPresenter.SERVICE_OPEN) {
            serviceStarted()
            // makeSnackbar(getString(R.string.service_started))
        } else {
            serviceStopped()
            //makeSnackbar(getString(R.string.service_stoppped))
        }
    }

    override fun setServiceInfo(model: DNSModel?) {

    }

    fun generateToken(mCallback: NetsikringFragment.onTokenGenrateCallBack) {
        showLoader()
        val api = getApiService()!!.getToken()
        val connector = ApiConnector<GetTokenResponse>(
            this@HomeActivity,
            api,
            GetTokenResponse::class.java
        )
        connector.initApi(object : ApiConnector.onApiCallBack<GetTokenResponse> {
            override fun onSuccess(response: GetTokenResponse) {
                mCallback.getToken(response.token!!)
            }

            override fun onFailure(message: String) {
                showInformationDialog(message)
                hideLoader()
            }
        })
    }

    fun optionsDialog() {
        val binding = DataBindingUtil.inflate<DialogOptionsBinding>(
            LayoutInflater.from(this),
            R.layout.dialog_options,
            null,
            false
        )
        val dialog = BottomSheetDialog(this, R.style.AppBottomSheetDialogTheme)
        dialog.setContentView(binding.root)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)
        if (getViewDataBinding().internetStatus.visibility == View.VISIBLE) {
            binding.tvChangePassword.hide()
            binding.tvLogout.hide()
            binding.tvLock.hide()
        } else {
            binding.tvChangePassword.show()
            binding.tvLogout.show()
            binding.tvLock.show()
        }
        binding.tvChangePassword.setOnClickListener {
            dialog.dismiss()
            val intent = Intent(this@HomeActivity, ActivityChangePassword::class.java)
            startActivity(intent)
            overridePendingTransition(
                R.anim.fade_in,
                R.anim.fade_out
            )
        }
        binding.tvAboutUs.setOnClickListener {
            dialog.dismiss()

            getNetsikringPrefs()!!.getConfigurations(this@HomeActivity)!!.list!!.withIndex().map {
                if (it.value.keyName!!.equals("company_info", true)) {
                    showInformationDialog(it.value.value!!)

                }
            }

        }
        binding.tvPurchasePlan.setOnClickListener {
            startActivity(Intent(this@HomeActivity, SubscriptionActivity::class.java))
        }
        binding.tvLogout.setOnClickListener {
            dialog.dismiss()
            if (getViewDataBinding().internetStatus.visibility == View.VISIBLE) {
                showToast("Frakoblet. Beskyttelsen er sat på pause")
            } else {
                showLogoutDialog()
            }

        }
        binding.tvLock.setOnClickListener {

            if (!dpm.isAdminActive(deviceAdminSample)) {
                startAdminRequest()
            } else {

                //enableDeviceCapabilitiesArea(false)
            }
            dialog.dismiss()
        }
        dialog.show()

    }

    private fun startAdminRequest() {
        val intent = Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN).apply {
            putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, deviceAdminSample)
            putExtra(
                DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                "Disabel application"
            )
        }
        startActivityForResult(intent, REQUEST_CODE_ENABLE_ADMIN)
    }

    val REQUEST_CODE_ENABLE_ADMIN = 1

    fun showInformationDialog(message: String) {
        NetsikringDialog<DialogInformationBinding>(this@HomeActivity,
            R.layout.dialog_information,
            object : DialogBinding<DialogInformationBinding> {
                override fun onBind(binder: DialogInformationBinding, dialog: Dialog) {
                    binder.apply {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            tvMessage.text = Html.fromHtml(message, Html.FROM_HTML_MODE_LEGACY)
                        } else {
                            tvMessage.text = Html.fromHtml(message)
                        }

                        tvMessage.movementMethod = ScrollingMovementMethod()
                        tvUnderstand.setOnClickListener {
                            dialog!!.dismiss()
                        }
                    }

                }
            }).show(supportFragmentManager, "showInformationDialogr")
    }

    fun showEnterPin() {
        NetsikringDialog<DialogEnterPinToStopVpnBinding>(this@HomeActivity,
            R.layout.dialog_enter_pin_to_stop_vpn,
            object : DialogBinding<DialogEnterPinToStopVpnBinding> {
                override fun onBind(binder: DialogEnterPinToStopVpnBinding, dialog: Dialog) {
                    binder.apply {
                        val pin = getNetsikringPrefs()!!.getUserPin(this@HomeActivity)
                        tvStop.setOnClickListener {
                            val ePin = binder.etPin.text.toString().trim()
                            if (ePin == pin) {
                                dialog.dismiss()
                                updateServiceStatus("0")


                            } else {
                                showToast("Indtast korrekt PIN-kode")
                            }
                        }
                    }

                }
            }).show(supportFragmentManager, "showInformationDialog")
    }

    private fun checkIfPinIsChanged() {
        //  showLoader()
        generateToken(object : NetsikringFragment.onTokenGenrateCallBack {
            override fun getToken(token: String) {
                val map = HashMap<String, Any>()
                map["_token"] = token
                map["email"] = getNetsikringPrefs()!!.getEmail(this@HomeActivity)!!
                map["password"] = getNetsikringPrefs()!!.getUserPin(this@HomeActivity)!!
                map["device_id"] = AppCommon.getDeviceID(this@HomeActivity)
                map["device_token"] = getNetsikringPrefs()?.getDeviceToken(this@HomeActivity) ?: ""

                val api = getApiService()!!.doLogin(map)
                val connector = ApiConnector<LoginResponse>(
                    this@HomeActivity,
                    api,
                    LoginResponse::class.java
                )
                connector.initApi(object : ApiConnector.onApiCallBack<LoginResponse> {
                    override fun onSuccess(response: LoginResponse) {
                        hideLoader()
                        if (!response.success!!) {
                            updateServiceStatus("0")
                            GlobalScope.launch {
                                delay(1000)
                                this@HomeActivity.runOnUiThread {


                                    getNetsikringPrefs()!!.isUserLoggedIn(this@HomeActivity, false)
                                    navigateToActivity()
                                }
                            }

                        } else {
                            showEnterPin()
                        }
                    }

                    override fun onFailure(message: String) {

                    }
                })
            }
        })
    }

    fun differenceBetweenDate(): Int {
        try {
            //Dates to compare
            val CurrentDate = getNetsikringPrefs()?.getUserCreateAt(this@HomeActivity)
            val FinalDate = getCurrentUTCDate()
            val date1: Date
            val date2: Date
            val dates = SimpleDateFormat("yyyy-MM-dd")

            //Setting dates
            date1 = dates.parse(CurrentDate)
            date2 = dates.parse(FinalDate)

            //Comparing dates
            val difference: Long = Math.abs(date1.getTime() - date2.getTime())
            val differenceDates = difference / (24 * 60 * 60 * 1000)

            //Convert long to String
            val dayDifference = java.lang.Long.toString(differenceDates)
            return differenceDates.toInt()
            L.e("HERE", "HERE: $dayDifference")
        } catch (exception: java.lang.Exception) {
            L.e("DIDN'T WORK", "exception $exception")
            return 0
        }
    }


    fun getCurrentUTCDate(): String? {
        val format = "yyyy-MM-dd"
        val sdf = SimpleDateFormat(format)
        sdf.timeZone = TimeZone.getTimeZone("UTC");
        val dateInMillis = System.currentTimeMillis()
        val date = sdf.format(Date(dateInMillis))
        return date
    }

}