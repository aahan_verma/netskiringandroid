package com.netsikring.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.netsikring.R
import com.netsikring.databinding.FragmentHomeBinding
import com.netsikring.di.model.ToolBarConfiguration
import com.netsikring.ui.netsikring.NetsikringFragment
import com.netsikring.utils.hide
import com.netsikring.utils.show

class HomeFragment : NetsikringFragment<FragmentHomeBinding>() {
    override fun getLayoutId(): Int {
        return R.layout.fragment_home
    }

    override fun getCurrentFragment(): Fragment? {
        return this@HomeFragment
    }

    override fun showToolbar(): Boolean? {
        return true
    }

    override fun configureToolbar(): ToolBarConfiguration? {
        return ToolBarConfiguration(R.drawable.ic_baseline_apps_24, "")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (getNetsikringPrefs()!!.getReceiverConnected(getContainerActivity())){
            serviceStarted()
        }else{
            serviceStopped()
        }
    }


    fun serviceStopped() {
       //
        getViewDataBinding().tvConnectionStatus.text = "Connect"
        getViewDataBinding().internetStatus.hide()
        getContainerActivity().runOnUiThread {
            getNetsikringPrefs()!!.isReceiverConnected(getContainerActivity(),false)
        }
    }

    fun serviceStarted() {

        getViewDataBinding().tvConnectionStatus.text = "Connected"
        getViewDataBinding().internetStatus.show()
        getContainerActivity().runOnUiThread {
            getNetsikringPrefs()!!.isReceiverConnected(getContainerActivity(),true)
        }
    }
}