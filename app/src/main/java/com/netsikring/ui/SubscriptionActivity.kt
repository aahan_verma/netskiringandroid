package com.netsikring.ui

import com.netsikring.base.BaseActivity
import com.netsikring.R
import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.View
import com.netsikring.base.adapter.RecyclerCallback
import com.netsikring.base.adapter.RecyclerViewGenericAdapter
import com.netsikring.databinding.ActivityPurachseSubscriptionBinding
import com.netsikring.databinding.DialogErrorBinding
import com.netsikring.databinding.ItemAdapterSubscriptionBinding
import com.netsikring.model.PackageClass
import com.netsikring.ui.netsikring.NetsikringDialog
import com.ntpc.app.base.DialogBinding
import java.lang.Exception
import android.content.Intent
import android.net.Uri
import android.util.Log
import com.netsikring.databinding.DialogSubscriptionBoughtBinding
import com.netsikring.model.GetTokenResponse
import com.netsikring.network.ApiConnector
import com.netsikring.ui.netsikring.NetsikringFragment


class SubscriptionActivity : BaseActivity<ActivityPurachseSubscriptionBinding>() {
    override fun getLayoutId(): Int {
        return R.layout.activity_purachse_subscription
    }

    override fun getCurrentActivity(): Activity? {
        return this
    }

    var mAdapter: RecyclerViewGenericAdapter<PackageClass, ItemAdapterSubscriptionBinding>? = null
    var mSubList = ArrayList<PackageClass>()
    var clickedPos = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewDataBinding().apply {
            mSubList =getPackageList()
            mAdapter = RecyclerViewGenericAdapter(mSubList,R.layout.item_adapter_subscription,
            object :RecyclerCallback<ItemAdapterSubscriptionBinding,PackageClass>{
                override fun bindData(
                    binder: ItemAdapterSubscriptionBinding,
                    model: PackageClass,
                    position: Int,
                    itemView: View?
                ) {
                    binder.apply {
                        rbButton.isChecked = clickedPos == position
                        root.setOnClickListener {
                            if (!model.isTaken) {
                                clickedPos = position
                                mAdapter!!.notifyDataSetChanged()
                            }else{
                                clickedPos =-1
                              showAlreadyBoughtAlert(model)
                            }
                        }
                        if (model.isTaken){
                            rbButton.visibility = View.INVISIBLE
                        }
                        rbButton.setOnCheckedChangeListener { buttonView, isChecked ->
                            if (!model.isTaken) {
                                if (buttonView.isPressed){
                                    clickedPos = position
                                    mAdapter!!.notifyDataSetChanged()
                                }
                            }else{
                                clickedPos =-1
                                showAlreadyBoughtAlert(model)
                            }

                        }
                        tvTitle.text = "${model.price}"
                        tvTitlePrice.text = "${model.title}"
                    }

                }
            })
            rvSubscriptionPackage.adapter=mAdapter
            btnSubscribe.setOnClickListener {
                if (clickedPos == -1){
                    showToast("Please select subscription package")
                }else{
                    val obj = mSubList[clickedPos]
                    goForSubScriptingPurchase(obj.packageId,object :BaseActivity.onPurchaseComplete{
                        override fun onComplete() {
                            uploadPurchaseToken(obj.packageId)

                        }
                    })
                }
            }
            ivNav.setOnClickListener {
                onBackPressed()
            }
        }
    }

    private fun showAlreadyBoughtAlert(model: PackageClass) {
        NetsikringDialog<DialogSubscriptionBoughtBinding>(
            this@SubscriptionActivity,
            R.layout.dialog_subscription_bought,
            object : DialogBinding<DialogSubscriptionBoughtBinding> {
                override fun onBind(binder: DialogSubscriptionBoughtBinding, dialog: Dialog) {

                    binder.btnClick.setOnClickListener {
                        dialog.dismiss()
                        val browserIntent = Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/account/subscriptions")
                        )
                        startActivity(browserIntent)
                    }
                    binder.tvNowNow.setOnClickListener {
                        dialog.dismiss()
                    }
                }
            }).show(supportFragmentManager, NetsikringDialog::class.java.simpleName)
    }


    fun getPackageList():ArrayList<PackageClass>{
        val mSubList = ArrayList<PackageClass>()
        mSubList.add(PackageClass("com.netsikring.permium","DKK 59.00","Netsikker","Premium",getIsTaken("Premium")))
        mSubList.add(PackageClass("com.netsikring.kombi_1","DKK 99.00","Kombi 1","Kombi 1",getIsTaken("Kombi 1")))
        mSubList.add(PackageClass("com.netsikring.kombi_3","DKK 199.00","Kombi 3","kombi 3",getIsTaken("kombi 3")))
        mSubList.add(PackageClass("com.netsikring.kombi_6","DKK 299.00","Kombi 6","kombi_6",getIsTaken("kombi_6")))
        mSubList.add(PackageClass("com.netsikring.ultimate_20","DKK 999.00","Ultimate 20","ultimate_20",getIsTaken("ultimate_20")))
        return mSubList
    }

    private fun getIsTaken(s: String): Boolean {
        return try {
            getNetsikringPrefs()?.getTakenProgram(this@SubscriptionActivity).equals("$s",true)
        }catch (e:Exception){
            e.printStackTrace()
            false
        }
    }

    private fun uploadPurchaseToken(selectedSKu: String) {
        generateT(object : NetsikringFragment.onTokenGenrateCallBack {
            override fun getToken(token: String) {
                val map = HashMap<String, Any>()
                map["_token"] = token
                map["email"] = getNetsikringPrefs()!!.getUserInformation(this@SubscriptionActivity)!!.email ?: ""
                map["subscription_plan"] = getSubscriptionPlan(selectedSKu, true)
                map["maximum_devices"] = getSubscriptionPlan(selectedSKu, false)
                val api = getApiService()!!.updateSubscriptionPlan(map)
                val connector = ApiConnector<GetTokenResponse>(
                    this@SubscriptionActivity,
                    api,
                    GetTokenResponse::class.java
                )
                connector.initApi(object : ApiConnector.onApiCallBack<GetTokenResponse> {
                    override fun onSuccess(response: GetTokenResponse) {
                        if (response.success!!) {
                            val email = getEmail()
                            val planName = getSubscriptionPlan(selectedSKu,true)
                            Log.e("PlanName",planName)
                            getFirebaseDBInstance().child(email)
                                .setValue(planName)
                            getNetsikringPrefs()?.setTakenProgram(this@SubscriptionActivity,planName)
                            onBackPressed()
                        } else {
                            hideLoader()
                            showToast(response.message ?: response.error ?: "")
                        }
                    }

                    override fun onFailure(message: String) {
                        hideLoader()
                    }
                })

            }
        })
    }

    fun getSubscriptionPlan(selectedSKu: String, isPackage: Boolean): String {
        if (selectedSKu.equals("com.netsikring.permium")) {
            if (isPackage) {
                return "Premium"
            } else {
                return "1"
            }
        } else if (selectedSKu.equals("com.netsikring.kombi_6")) {
            if (isPackage) {
                return "kombi_6"
            } else {
                return "6"
            }
        } else if (selectedSKu.equals("com.netsikring.ultimate_20")) {
            if (isPackage) {
                return "ultimate_20"
            } else {
                return "20"
            }
        } else if (selectedSKu.equals("com.netsikring.kombi_3")) {
            if (isPackage) {
                return "kombi 3"
            } else {
                return "3"
            }
        } else if (selectedSKu.equals("com.netsikring.kombi_1")) {
            if (isPackage) {
                return "Kombi 1"
            } else {
                return "1"
            }
        }else{
            return ""
        }
    }
}