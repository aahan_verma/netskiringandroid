package com.netsikring.ui.profile

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.netsikring.R
import com.netsikring.databinding.FragmentCreateProfileBinding
import com.netsikring.di.model.ToolBarConfiguration
import com.netsikring.model.GetTokenResponse
import com.netsikring.model.LoginResponse
import com.netsikring.network.ApiConnector
import com.netsikring.ui.home.HomeActivity
import com.netsikring.ui.netsikring.AppPresenter
import com.netsikring.ui.netsikring.NetsikringFragment
import com.netsikring.utils.AppCommon
import com.netsikring.utils.loadGlide

class CreateProfileFragment : NetsikringFragment<FragmentCreateProfileBinding>() {
    override fun getLayoutId(): Int {
        return R.layout.fragment_create_profile
    }

    override fun getCurrentFragment(): Fragment? {
        return this@CreateProfileFragment
    }

    override fun showToolbar(): Boolean? {
        return false
    }

    override fun configureToolbar(): ToolBarConfiguration? {
        return null
    }

    var appPresenter: AppPresenter? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (appPresenter == null) {
            appPresenter = AppPresenter(getContainerActivity(), this@CreateProfileFragment)
            getViewDataBinding().tvSigIn.setOnClickListener {
                getContainerActivity().onBackPressed()
            }

            getViewDataBinding().btnNext.setOnClickListener {
                if (isEmailValid(
                        getViewDataBinding().etEmail,
                        "Indtast gyldig email adresse"
                    ) && validateEditText(
                        getViewDataBinding().etName,
                        "Indtast dit navn"
                    ) &&
                    validateEditText(getViewDataBinding().etPassword, "PIN-kode")
                    && comparePassword(
                        getViewDataBinding().etPassword,
                        getViewDataBinding().etCPassword
                    )
                ) {
                    /*getNetsikringPrefs()!!.isUserLoggedIn(getContainerActivity(), true)
                    val intent = Intent(getContainerActivity(), HomeActivity::class.java)
                    getContainerActivity().startActivity(intent)
                    getContainerActivity().overridePendingTransition(
                        R.anim.fade_in,
                        R.anim.fade_out
                    )
                    getContainerActivity().finishAffinity()*/
                    register()
                }
            }
            getViewDataBinding().ivProfile.setOnClickListener {
                appPresenter!!.onProfileImageClick(1000, "Select Profile Image")
            }
        }
    }

    private fun register() {
        generateToken(object : NetsikringFragment.onTokenGenrateCallBack {
            override fun getToken(token: String) {
                val map = HashMap<String, Any>()
                map["_token"] = token
                map["email"] = getViewDataBinding().etEmail.text.toString().trim()
                map["password"] = getViewDataBinding().etPassword.text.toString().trim()
                map["name"] = getViewDataBinding().etName.text.toString().trim()
                map["device_id"] = AppCommon.getDeviceID(getContainerActivity())
                map["model"] =  Build.MODEL
                map["device_token"] = getContainerActivity().getNetsikringPrefs()?.getDeviceToken(getContainerActivity()) ?: ""

                val api = getApiService()!!.register(map)
                val connector = ApiConnector<LoginResponse>(
                    getContainerActivity(),
                    api,
                        LoginResponse::class.java
                )
                connector.initApi(object : ApiConnector.onApiCallBack<LoginResponse> {
                    override fun onSuccess(response: LoginResponse) {
                        hideLoading()
                        if (response.success!!) {
                            showToast("Velkommen!")
                            getNetsikringPrefs()!!.setUserCreateAt(
                                getContainerActivity(),
                                getCurrentUTCDate()!!
                            )
                            getNetsikringPrefs()!!.setUserInformation(
                                    getContainerActivity(),
                                    response.user!!
                            )
                            getNetsikringPrefs()!!.isUserLoggedIn(getContainerActivity(), true)
                            getNetsikringPrefs()!!.setUserPin(
                                getContainerActivity(),
                                getViewDataBinding().etPassword.text.toString().trim()
                            )
                            getNetsikringPrefs()!!.setEmail(
                                getContainerActivity(),
                                getViewDataBinding().etEmail.text.toString().trim()
                            )
                            val intent = Intent(getContainerActivity(), HomeActivity::class.java)
                            getContainerActivity().startActivity(intent)
                            getContainerActivity().overridePendingTransition(R.anim.fade_in,R.anim.fade_out)
                            getContainerActivity().finishAffinity()
                        } else {
                            showError(response.message!!)
                        }
                    }

                    override fun onFailure(message: String) {
                        showError(message)
                        hideLoading()
                    }
                })
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                val uri: Uri = data!!.getParcelableExtra("path")!!
                // profileImageFile = File(uri.path!!)
                loadGlide(uri.path!!, getViewDataBinding().ivProfile, getContainerActivity())

            }
        }
    }
}