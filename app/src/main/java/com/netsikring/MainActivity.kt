package com.netsikring

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.netsikring.base.BaseActivity
import com.netsikring.databinding.ActivityMainBinding
import com.netsikring.model.DNSModel
import com.netsikring.ui.home.HomeActivity
import com.netsikring.ui.netsikring.NetsikringActivity
import com.netsikring.utils.loadGlide

class MainActivity : BaseActivity<ActivityMainBinding>() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
       // loadGlide(R.drawable.ic_splash_logo,getViewDataBinding().ivSplash,this@MainActivity)

        Glide.with(this@MainActivity).load(R.drawable.ic_splash_logo).into(getViewDataBinding().ivSplash)
        Handler(Looper.getMainLooper()).postDelayed({
            navigateToActivity()
        }, 2500)

    }

    fun navigateToActivity() {
        if (getNetsikringPrefs()!!.getIsUserLoggedIn(this@MainActivity)!!) {
            val intent = Intent(this@MainActivity, HomeActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finishAffinity()

        } else {
            val intent = Intent(this@MainActivity, NetsikringActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finishAffinity()
        }

    }


    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun getCurrentActivity(): Activity? {
        return this@MainActivity
    }
}