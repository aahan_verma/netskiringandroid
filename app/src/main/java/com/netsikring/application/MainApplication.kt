package com.netsikring.application

import android.app.Application
import androidx.multidex.MultiDexApplication
import com.netsikring.di.component.ApplicationComponent
import com.netsikring.di.component.DaggerApplicationComponent
import com.netsikring.di.module.ApplicationModule

class MainApplication : MultiDexApplication() {
    companion object{
        fun getApplicationComponent(): ApplicationComponent? {
            return applicationComponent
        }
        private var applicationComponent: ApplicationComponent? = null
    }



    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }
}